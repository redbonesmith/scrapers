# Scrapers

### Install
```bash
$ git clone git@bitbucket.org:redbonesmith/scrapers.git
$ cd scrapers
$ npm install
```

### Usage
```bash
$ ./bin/scrapers
```

```bash
$ npm start
```

- goto: http://localhost:7834/scrape/?ttn=$TTN
- Where $TTN is take from here : https://goo.gl/atgC7n

### Isues
- [deprecated]https://bitbucket.org/redbonesmith/scrapers/issues?status=new&status=open

- All issues on *pivotal*

### WIKI
https://bitbucket.org/redbonesmith/scrapers/wiki/Home