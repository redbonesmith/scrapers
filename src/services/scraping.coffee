_ = require 'lodash'
mongoose = require 'mongoose'
cheerio = require 'cheerio'
config = require '../config/config'
Promise = require 'bluebird'
ctl = require 'cyrillic-to-latin'
validator = require 'validator'
request = Promise.promisify(require 'request')


clientError = (e) ->
  console.trace e
  return e.code >= 400 && e.code < 500

matchString = (str, where) ->
  match = /(str)/
  return match.test where

cleanText = (word) ->
  if word?
    return word.trim()
  else
    console.error 'Not a word'

parseData = (html) ->
  info = []
  options=
    normalizeWhitespace: false,
    xmlMode: false,
    decodeEntities: true

  $ = cheerio.load(html)

  $('div.highlight p').each ->
    cleanWords = $(this)
      .text()
      .split(':')
      .map(cleanText)

    info.push {
      key: cleanWords[0]
      value: cleanWords[1]
    }
    # this line needed for location finding
    $(this).remove()

  info = info[0..4]
  probablyLocation = $('div.highlight')
    .text()
    .split('\n')[2..3]
    .map cleanText

  info.push {
    status: probablyLocation[1]
  } if probablyLocation[0] == 'Current location:'
  return info

ruleEstimateDeliveryDate = (el) ->
  estimatedDate: el.value.replace(/\./g, '-')

ruleAddress = (el) ->
  deliveryStorageNumber: el.value.split(' ')[1].replace('№', '')

ruleWarnMsg = (el) ->
  # if UAH in el.value => this might be a delivery cost
  return {
      status: {
        key: 'status'
        value: 'delivered'
      }
    } unless el.value?
  return msg: cleanText el.key if el.key
  return msg: cleanText el.value if el.value

ruleForRoute = (el) ->
  towns = el.value.split('-')
  route =
    from: ctl cleanText towns[0]
    to: ctl cleanText towns[1]
  return {route}

ruleStatus = (el) ->
  # @todo: logic for parsing status
  # for now just return value
  status =
    key: 'status'
    value: el.status
  return {status}

ruleRouter = (el) ->
  if el.value?
    return ruleEstimateDeliveryDate el if matchString 'date', el.key
    return ruleForRoute el if el.key == 'Route'
    return ruleAddress el if matchString 'address', el.key
    return ruleStatus el if el.status?
    return {}
  else
    return ruleWarnMsg el

scrapeData = Promise.method (ttn, res) ->
  # console.log res.req.headers, 'headers'
  request "#{config.NEWPOSTLINK}#{ttn}"
    .spread (res, html) ->
      data = parseData html
      info = []
      statusCounter = 0
      for el in data
        # BB-3 issue:
        # fixMe: this fugly - duplicate remover
        # problem: info:
        # [ { route: { from: 'Kiїv', to: 'Lʹvіv' } },
        # { status: { key: 'status', value: 'delivered' } },
        # { status: { key: 'status', value: 'delivered' } } ]
        # wrong status assigning
        statusCounter++ if el.status?
        if statusCounter == 0
          result = ruleRouter(el)
          info.push result unless _.isEmpty result

      return info
    .catch clientError

exports.scrape = (req, res, next) ->
  return res.status(400).json(error: 'no ttn') unless req.query.ttn?

  ttnValidator = (_ttn) ->
    hasCorrectLen = _ttn.length == 11 or _ttn.length == 14
    isNumeric = validator.isNumeric _ttn
    return hasCorrectLen and isNumeric

  ttn = req.query.ttn

  if ttnValidator(ttn)
    scrapeData ttn, res
      .then (rendered) ->
        doc=
          ttn: ttn
          data: rendered
        res.setHeader('Content-Type', 'application/json')
        res.setHeader('charset', 'utf-8')
        res.json doc
      .catch (err) ->
        console.error {err}, 'scrapeData error'
        return next err
