mongoose = require 'mongoose'
mongoose.Promise = require('bluebird')

config = require './config/config'
mongoose.set 'debug', config.MONGOOSE_DEBUG
mongoose.connect config.MONGODB_URI

db = mongoose.connection

db.on 'error', (err) ->
  console.error {err} if err?
