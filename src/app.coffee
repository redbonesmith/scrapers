# log = require('./log').child(type: 'app')

express = require 'express'
bodyParser = require 'body-parser'
path = require 'path'
cookieParser = require 'cookie-parser'
routes = require './routes'
cors = require 'cors'
serveStatic = require 'serve-static'
logger = require 'morgan'
config = require './config/config'

# open database connection
require './database'

app = express()

app.set 'trust proxy', ['loopback', 'linklocal', 'uniquelocal']


app.use bodyParser.json()
app.use bodyParser.urlencoded(extended: true)

app.use logger 'dev'
app.use cors(origin: true)
app.use routes
app.use cookieParser()
app.use serveStatic(path.join(__dirname, '/client/'))

env = process.env.NODE_ENV || 'development'
app.locals.ENV = env
app.locals.ENV_DEVELOPMENT = env == 'development'


# error handlers

# catch 404 and forward to error handler
app.use (req, res, next) ->
  err = new Error 'Not Found'
  err.status = 404
  next err


# development error handler
# will print stacktrace

if app.get('env') == 'development'
  app.use (err, req, res, next) ->
    res.status err.status || 500
    res.render 'error',
      message: err.message
      error: err
      title: 'error'

# production error handler
# no stacktraces leaked to user
app.use (err, req, res, next) ->
  res.status err.status || 500
  res.render 'error',
    message: err.message
    error: {}
    title: 'error'

module.exports = app
