express = require 'express'
scrapingService = require './services/scraping'

router = express.Router()

router.get '/', (req, res) ->
  res.render('./../client/index.html')

router.get '/scrape', scrapingService.scrape

module.exports = router
